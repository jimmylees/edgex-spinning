package main

import (
	"fmt"
	"os"

	"edgex-spinning/spinning"

	appsdk "github.com/edgexfoundry/app-functions-sdk-go/v2/pkg"
)

const (
	serviceKey = "app-spinning-management"
)

func main() {

	os.Setenv("EDGEX_SECURITY_SECRET_STORE", "false")

	service, ok := appsdk.NewAppService(serviceKey)
	if !ok {
		fmt.Printf("error: unable to create new app service %s!\n", serviceKey)
		os.Exit(-1)
	}

	app := spinning.NewSpinningApp(service)
	if err := app.Run(); err != nil {
		service.LoggingClient().Error(err.Error())
		os.Exit(-1)
	}

	os.Exit(0)
}
