// -*- Mode: Go; indent-tabs-mode: t -*-
//
// Copyright (C) 2018 IOTech Ltd
//
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/url"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

const (
	brokerUrl  = "192.168.0.248"
	brokerPort = 1883
	username   = "admin"
	password   = "public"
)

func main() {
	var mqttClientId = "mqttDevice001"
	uri := &url.URL{
		Scheme: "tcp",
		Host:   fmt.Sprintf("%s:%d", brokerUrl, brokerPort),
		User:   url.UserPassword(username, password),
	}

	client, err := createMqttClient(mqttClientId, uri)	
	if err != nil {
		fmt.Println(err)
	}

	defer client.Disconnect(5000)

	go runCommandHandler(client)
	go runDataSender(client)
	select {}
}

// runCommandHandler use to test receiving commands from the device service and responded back for get/set commands.
//
// Use a REST client to send a command to the service like:
// http://localhost:59982/api/v1/devices/{device id}>/message - use POST on this one with
// {"message":"some text"} in body http://localhost:59982/api/v1/devices/<device id>/ping - use GET
// http://localhost:59982/api/v1/devices/<device id>/randnum - use GET
//
// If command micro service is running, the same can be performed through command to device service
// like this http://localhost:59882/api/v1/device/<device id>/command/<command id>
//
// Requires the Device Service, Command, Core Data, Metadata and Mongo to all be running
func runCommandHandler(client mqtt.Client) {
	
	var qos = 0
	var topic = "CommandTopic"

	token := client.Subscribe(topic, byte(qos), onCommandReceivedFromBroker)
	fmt.Println("### runCommandHandler - Subscribe ###\n")
	if token.Wait() && token.Error() != nil {
		fmt.Println()
	}

	select {}
}

// runDataSender use to to generate random numbers and send them into the device service as if a sensor
// was sending the data. Requires the Device Service along with Mongo, Core Data, and Metadata to be running
func runDataSender(client mqtt.Client) {

	var qos = byte(0)
	var topic = "DataTopic"

	var data = make(map[string]interface{})
	data["name"] = "MQTT test device"
	data["cmd"] = "randnum"
	data["method"] = "get"

	for {
		data["randnum"] = rand.Float64() //nolint:gosec
		jsonData, err := json.Marshal(data)
		if err != nil {
			fmt.Println(err)
		}
		client.Publish(topic, qos, false, jsonData)

		fmt.Printf("### runDataSender - Send response: %v ###\n", string(jsonData))

		time.Sleep(time.Second * time.Duration(30))
	}

}

func onCommandReceivedFromBroker(client mqtt.Client, message mqtt.Message) {
	var request map[string]interface{}

	err := json.Unmarshal(message.Payload(), &request)
	if err != nil {
		fmt.Printf("Error unmarshaling payload: %s\n", err)
		return
	}
	uuid, ok := request["uuid"]
	if ok {
		fmt.Printf("Command response received: topic=%v uuid=%v msg=%v\n", message.Topic(), uuid, string(message.Payload()))

		if request["method"] == "set" {
			sendTestData(request, client)
		} else {
			switch request["cmd"] {
			case "ping":
				request["ping"] = "pong"
				sendTestData(request, client)
			case "randfloat32":
				request["randfloat32"] = rand.Float32() //nolint:gosec
				sendTestData(request, client)
			case "randfloat64":
				request["randfloat64"] = rand.Float64() //nolint:gosec
				sendTestData(request, client)
			case "message":
				request["message"] = "test-message"
				sendTestData(request, client)
			}
		}
	} else {
		log.Printf("Command response ignored. No UUID found in the message: topic=%v msg=%v\n", message.Topic(), string(message.Payload()))
	}
}

func sendTestData(response map[string]interface{}, client mqtt.Client) {
	var qos = byte(0)
	var topic = "ResponseTopic"

	jsonData, err := json.Marshal(response)
	if err != nil {
		fmt.Println(err)
	}
	client.Publish(topic, qos, false, jsonData)

	fmt.Printf("### sendTestData - Send response: %v ###\n", string(jsonData))
}

func createMqttClient(clientID string, uri *url.URL) (mqtt.Client, error) {
	fmt.Printf("### Create MQTT client and connection: uri=%v clientID=%v ###\n", uri.String(), clientID)
	opts := mqtt.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("%s://%s", uri.Scheme, uri.Host))
	opts.SetClientID(clientID)
	opts.SetUsername(uri.User.Username())
	password, _ := uri.User.Password()
	opts.SetPassword(password)
	opts.SetConnectionLostHandler(func(client mqtt.Client, e error) {
		fmt.Printf("Connection lost : %v", e)
		token := client.Connect()
		if token.Wait() && token.Error() != nil {
			fmt.Printf("Reconnection failed : %v", e)
		} else {
			fmt.Printf("Reconnection sucessful : %v", e)
		}
	})

	client := mqtt.NewClient(opts)
	fmt.Println("### createMqttClient ###")
	token := client.Connect()
	if token.Wait() && token.Error() != nil {
		return client, token.Error()
	}

	return client, nil
}
