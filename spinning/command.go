package spinning

type EdgexCmd struct {
	// Edgex 向MCU下发命令的格式  MQTT消息

	// 命令：voltages - 锭子电压数据波形图，命令：settings - MCU配置
	Cmd string `json:"cmd"`

	// 开始 时间戳， 用来判断是否超时
	Ts string `json:"ts"`

	Spindle int    `json:"spindle"` //  Cmd 为 voltages 时的 指定锭号
	Setting string `json:"setting"` //  Cmd 为 settings 时的 指定配置名
	Tid     int64  `json:"tid"`     //  Cmd 事务ID
	Status  int    `json:"status"`  //
}

func (c *EdgexCmd) Init() {
	c.Cmd = ""
	c.Ts = ""
	c.Spindle = 0
	c.Setting = ""
	c.Tid = 0

	// -1 进行中，0 初始状态， 1完成状态
	c.Status = 0
}

func (c *EdgexCmd) Copy(s *EdgexCmd) {
	c.Cmd = s.Cmd
	c.Ts = s.Ts
	c.Spindle = s.Spindle
	c.Setting = s.Setting
	c.Tid = s.Tid
	c.Status = s.Status
}
