package spinning

import (
	"github.com/go-redis/redis"
)

type ADO struct {
	addr string
	port int32
	rds  *redis.Client
}

func (ado *ADO) init() error {

	ado.rds = redis.NewClient(&redis.Options{
		Addr:     "127.0.0.1:6379",
		Password: "",
		DB:       0,
	})
	return nil
}
