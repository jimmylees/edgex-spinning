package spinning

func (d *Doffing) init(begin string) error {

	d.Begin = begin
	d.End = ""
	d.Duration = 0
	d.Spin_len = 0
	d.Spin_len_act = 0

	d.Actual_len = 0
	d.theory_total0 = 0
	d.actual_total1 = 0
	d.Theory_weight = 0
	d.Actual_weight = 0

	d.Efficiency = 0
	d.Progress = 0

	d.Brokens = 0
	d.Brokens_boot = 0
	d.Brokens_ord = 0
	d.Brokens_doff = 0

	d.Noocupations = 0
	d.Weaks = 0
	d.Malfunctions = 0
	d.Passbys = 0

	d.Broken_loss = 0
	d.Broken_fixed = 0
	d.Broken_duration = 0
	d.Keep_rate = 0
	d.Thousands_rate = 0

	return nil
}
