package spinning

type Doffing struct {
	ID       int64  `gorm:"primaryKey"`
	Begin    string `json:"begin"`
	End      string `json:"end"`
	Duration int    `json:"duration"`

	Spin_len     int `json:"spin_len"`
	Spin_len_act int `json:"spin_len_act"` // spin_len_actual

	Theory_len    int `json:"theory_len"`
	theory_total0 int
	theory_total1 int

	Actual_len    int `json:"actual_len"`
	actual_total0 int
	actual_total1 int

	Theory_weight int `json:"theory_weight"`
	Actual_weight int `json:"actual_weight"`

	Efficiency int `json:"efficiency"`
	Progress   int `json:"progress"`

	Brokens      int `json:"brokens"`
	Brokens_boot int `json:"brokens_boot"`
	Brokens_ord  int `json:"broken_ord"`
	Brokens_doff int `json:"brokens_doff"`

	Noocupations int `json:"noocupations"`
	Weaks        int `json:"weaks"`
	Malfunctions int `json:"malfunctions"`
	Passbys      int `json:"passbys"`

	Broken_loss     int `json:"broken_loss"`
	Broken_fixed    int `json:"broken_fixed"`
	Broken_duration int `json:"broken_duration"`

	Keep_rate      float32 `json:"keep_rate"`
	Thousands_rate float32 `json:"thousands_rate"`
}
