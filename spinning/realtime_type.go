package spinning

// McuRealtime 结构体
// 每隔3/5秒定时发送来机台状态-产量消息， 间隔多久应可以配置， 状态发生变化时也主动发，
type McuRealtime struct {
	ID          int64 `gorm:"primaryKey"`
	Status      int   `json:"status"`
	Front_speed int   `json:"front_speed"`
	Avg_speed   int   `json:"avg_speed"`

	Spin_len   int `json:"spin_len"`
	Theory_len int `json:"theory_len"`
	Actual_len int `json:"actual_len"`

	Spin_total   int `json:"spin_total"`
	Theory_total int `json:"theory_total"`
	Actual_total int `json:"actual_total"`

	Efficiency int `json:"efficiency"`
	Progress   int `json:"progress"`

	Brokens      int `json:"brokens"`
	Noocupations int `json:"noocupations"`
	Weaks        int `json:"weaks"`
	Malfunctions int `json:"malfunctions"`

	// 暂时不支持旁路
	Passbys int `json:"passbys"`

	Ts string `json:"ts"`

	upload      bool
	upload_time string
}

func (r *McuRealtime) copy_from(s *McuRealtime) {
	r.Status = s.Status
	r.Front_speed = s.Front_speed
	r.Avg_speed = s.Avg_speed
	r.Spin_len = s.Spin_len
	r.Theory_total = s.Theory_total
	r.Actual_total = s.Actual_total
	r.Efficiency = s.Efficiency
	r.Progress = s.Progress
	r.Brokens = s.Brokens
	r.Noocupations = s.Noocupations
	r.Weaks = s.Weaks
	r.Malfunctions = s.Malfunctions
	r.Passbys = s.Passbys
	r.Ts = s.Ts
	r.upload = false
	r.upload_time = ""
}

type status_log struct {
	Status int    `json:"status"`
	Ts     string `json:"ts"`
}
