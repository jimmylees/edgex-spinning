package spinning

type IndexResponse struct {
	Realtime McuRealtime `json:"realtime"`
	Settings McuSettings `json:"settings"`
	Spindles []Spindle   `json:"spindles"`
	Ts       string      `json:"ts"`
}
