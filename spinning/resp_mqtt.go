package spinning

type McuVoltages_Response struct {
	// MCU 发送的锭子电压数据波形图 MQTT消息内容格式

	// 帧协议头
	FrameIdentity uint16 `json:"frame_identity"` // 帧协议头标识  0xF5F5
	DataLen       uint16 `json:"data_len"`       // 数据部分长度
	//CrcCheck      [6]uint16 `json:"crc_check"`      // CRC 校验值

	// 帧协议 数据部分
	//Ts       string       `json:"ts"`
	Spindle  uint16       `json:"spindle"`  // 2bytes， 第6001 ~ 6002 字节
	Tid      uint64       `json:"tid"`      // Command 事务ID // 8bytes， 第 6003 ~ 6010字节
	Voltages [3000]uint16 `json:"voltages"` // 3000 * 2 = 6000bytes, 第1-6000字节
}

func (c *McuVoltages_Response) CheckFrame() bool {
	if c.FrameIdentity != 0xF5F5 {
		return false
	}

	return true
}
