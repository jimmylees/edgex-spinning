package spinning

type Schedule struct {
	ID    int64 `gorm:"primaryKey"`
	name  string
	date  string
	begin string
	end   string
}
