package spinning

// McuSettings 结构体， PLC配置信息
// 每次重启后， MCU主动发自身配置参数
type McuSettings struct {
	ID   int32  `gorm:"primaryKey"`
	Name string `json:"name"`

	Standard_twist       int    `json:"standard_twist"`
	Standard_front_speed int    `json:"standard_front_speed"`
	Spindle_mode         int    `json:"spindle_mode"`
	Spindle_count        int    `json:"spindle_count"`
	Plc_version          string `json:"plc_version"`
	Version              string `json:"version"`
	Boot_duration        int    `json:"boot_duration"`

	Boot_delay int    `json:"boot_delay"`
	Roving_cut int    `json:"roving_cut"`
	Dtime      string `json:"dtime"`

	upload      bool
	upload_time string
}

type McuSetting struct {
	// MCU 发送的配置参数 MQTT消息内容格式
	Setting string `json:"setting"`
	Value   uint16 `json:"value"`
}

type McuSettings_Response struct {
	// MCU 发送的配置参数 MQTT消息内容格式
	Settings []McuSetting `json:"settings"`
	Tid      uint64       `json:"tid"` // Command 事务ID
}
