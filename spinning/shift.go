package spinning

type Shift struct {
	name  string `gorm:"primaryKey"`
	begin string
	end   string
}