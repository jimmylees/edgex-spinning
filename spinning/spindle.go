package spinning

import (
	"encoding/json"
	"fmt"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	_ "github.com/mattn/go-sqlite3"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"

	"github.com/edgexfoundry/app-functions-sdk-go/v2/pkg/interfaces"
	"github.com/edgexfoundry/app-functions-sdk-go/v2/pkg/util"
)

func (app *SpinningApp) handleMcuSpindlesMsg(appContext interfaces.AppFunctionContext, data interface{}) (bool, interface{}) {
	// 处理 从 MCU 发送来的消息
	fmt.Println("### edgex/event/mcu/spindles begin ###")
	input, err := util.CoerceType(data)

	if err != nil {
		appContext.LoggingClient().Error(err.Error())
		return false, err
	}

	// 异常锭子处理
	// 将MCU发送来的数据 保存到 全局变量中：abnormalSpindle,,
	err2 := json.Unmarshal(input, &app.mcuRealtime)
	if err2 != nil {
		panic(err2)
	}

	// 保存到本地 数据库
	//spindle_db, err := sql.Open("sqlite3", "spindles.db")
	mcu_spindle_db, err3 := gorm.Open(sqlite.Open("mcu_spindle.db"), &gorm.Config{})
	if err3 != nil {
		appContext.LoggingClient().Info(err3.Error())
	}

	appContext.LoggingClient().Info("handleMcuSpindlesMsg")

	// 保存到本地 数据库
	mcu_spindle_db.Create(&app.mcuRealtime)

	var res = []byte{1, 2, 3}
	appContext.SetResponseData(res)

	fmt.Println("### edgex/event/mcu/spindles end ###")
	return false, nil
}

func (app *SpinningApp) handleMqttSpindlesMsg(client mqtt.Client, message mqtt.Message) {
	// 处理 从 MCU 发送来的消息
	fmt.Println("### edgex/event/mcu/spindles begin ###")

	// 异常锭子处理
	// 将MCU发送来的数据 保存到 全局变量中：abnormalSpindle,,
	var request map[string]interface{}
	err := json.Unmarshal(message.Payload(), &request)
	if err != nil {
		fmt.Printf("Error unmarshaling payload: %s\n", err)
		return
	}

	// 保存到本地 数据库
	//spindle_db, err := sql.Open("sqlite3", "spindles.db")
	mcu_spindle_db, err3 := gorm.Open(sqlite.Open("mcu_spindle.db"), &gorm.Config{})
	if err3 != nil {
		fmt.Println(err3.Error())
	}

	fmt.Println("handleMcuSpindlesMsg")

	// 保存到本地 数据库
	mcu_spindle_db.Create(&app.mcuRealtime)
}
