package spinning

// 实时 异常锭子信息;  // 保存到 数据库表中， 应分成 弱捻表，空置表，断头表，故障表， 旁路表
type Spindle struct {
	ID         int64 `gorm:"primaryKey"`
	doff_id    int64
	Spindle_no string `json:"spindle_no"`
	Status     int    `json:"status"`

	Ts    string `json:"ts"`
	Begin string `json:"begin"`
	End   string `json:"end"`
	Type  int    `json:"type"`

	upload      bool
	upload_time string
}

type Spindles struct {
	// MCU 发送的异常锭子 MQTT消息内容格式
	Brokens      []int  `json:"brokens"`
	Noocupations []int  `json:"noocupations"`
	Weaks        []int  `json:"weaks"`
	Malfunctions []int  `json:"malfunctions"`
	Ts           string `json:"ts"`
}

type Realtime struct {
	// MCU 发送的机台状态 MQTT消息内容格式
	Status       int    `json:"status"`
	Front_speed  int    `json:"front_speed"`
	Avg_speed    int    `json:"avg_speed"`
	Spin_len     int    `json:"spin_len"`
	Theory_len   int    `json:"theory_len"`
	Actual_len   int    `json:"actual_len"`
	Spin_total   int    `json:"spin_total"`
	Theory_total int    `json:"theory_total"`
	Actual_total int    `json:"actual_total"`
	Progress     int    `json:"progress"`
	Brokens      int    `json:"brokens"`
	Noocupations int    `json:"noocupations"`
	Weaks        int    `json:"weaks"`
	Malfunctions int    `json:"malfunctions"`
	Ts           string `json:"ts"`
}
