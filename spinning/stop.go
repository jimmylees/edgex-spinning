package spinning

type Stop struct {
	ID       int64  `gorm:"primaryKey"`
	Status   int    `json:"status"`
	Begin    string `json:"begin"`
	End      string `json:"end"`
	Duration int    `json:"duration"`
}

func (stop *Stop) init(begin string) error {

	return nil
}
