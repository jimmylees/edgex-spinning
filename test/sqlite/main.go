package main

import (
	"fmt"
	"os"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func main() {

	fmt.Println("### golang sqlite test begin ###")

	system_db, err := gorm.Open(sqlite.Open("../db/system.db3"), &gorm.Config{})
	if err != nil {
		fmt.Println(err.Error())
	}

	var setting = Setting{
		Name:                 "01",
		Standard_twist:       1,
		Standard_front_speed: 1,
		Spindle_mode:         1,
		Spindle_count:        1,
		Plc_version:          "v0.1",
		Version:              "v1",
		Boot_duration:        30,
		Boot_delay:           3,
		Roving_cut:           1,
		Ts:                   "",
		upload:               false,
		upload_time:          "",
	}
	system_db.Create(&setting)
	system_db.Commit()

	stop_db, err2 := gorm.Open(sqlite.Open("../db/stop.db3"), &gorm.Config{})
	if err2 != nil {
		fmt.Println(err2.Error())
	}

	var stop = Stop{
		Status: 2,
		Begin:  "2023-05-27 16:30:00",
	}
	stop_db.Create(&stop)
	fmt.Println(stop.ID)
	stop_db.Commit()

	fmt.Printf("%+v\n", &stop)
	fmt.Printf("%+v\n", &setting)
	fmt.Println("### golang sqlite test end ###")
	os.Exit(0)
}
