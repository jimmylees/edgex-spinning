package main

type Stop struct {
	ID       int64  `gorm:"primaryKey"`
	Status   int    `json:"status"`
	Begin    string `json:"begin"`
	End      string `json:"end"`
	Duration int    `json:"duration"`
}

type Shift struct {
	name  string
	begin string
	end   string
}

type Schedule struct {
	id    int64 `gorm:"primaryKey"`
	name  string
	date  string
	begin string
	end   string
}

type Setting struct {
	Name string `json:"name"`

	Standard_twist       int    `json:"standard_twist"`
	Standard_front_speed int    `json:"standard_front_speed"`
	Spindle_mode         int    `json:"spindle_mode"`
	Spindle_count        int    `json:"spindle_count"`
	Plc_version          string `json:"plc_version"`
	Version              string `json:"version"`
	Boot_duration        int    `json:"boot_duration"`

	Boot_delay int    `json:"boot_delay"`
	Roving_cut int    `json:"roving_cut"`
	Ts         string `json:"dtime"`

	upload      bool
	upload_time string
}

func (s *Setting) TableName() string {
	return "setting"
}

func (s *Stop) TableName() string {
	return "stop"
}
