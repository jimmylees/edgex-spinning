package main

import (
	"fmt"
	"math/big"
	"time"
)

func main() {
	current := time.Now()

	//time.Sleep(time.Microsecond * time.Duration(30))

	duration := time.Now().Sub(current) / 1000000000
	//duration := math.Floor(aa)
	fmt.Printf("### Duration:%d ###\n", duration)

	ts := current.Unix()
	fmt.Printf("### Duration:%s ###", time.Unix(ts, 0))

	//var a = (1392 / 4096) * 3300
	var a = big.NewFloat(1392)
	var b = big.NewFloat(4096)
	c, _ := new(big.Float).Quo(a, b).Float64()
	var _c = big.NewFloat(c)
	var d = big.NewFloat(3300)
	e, _ := new(big.Float).Mul(_c, d).Float32()
	fmt.Println(e)
	fmt.Println(uint16(e))
}
